# E-flips ReCaptcha
### Google ReCaptcha integration for Magento 1.9.x

#### Usage
* Setup store credentials [here](https://www.google.com/recaptcha/admin#list)
* Put "secret key" and "site key" on "System->Settings->(E-flips) ReCaptcha" in store backend
* In the template of forms that you want to put the ReCaptcha:
  * To render it:
    ```php
    <?= $this->getLayout()->createBlock('recaptcha/captcha')->toHtml() ?>
    ```
  * In forms that *aren't* contacts or customer register forms, put the following in first line of controller method that will handles the form submtion:
    ```php
    /**
     * Validate (re)captcha before continues the action
     */
    public function postAction()
    {
        $captcha = $this->getRequest()->getParam('g-recaptcha-response');

        if (!Mage::helper('recaptcha')->validateCaptcha($captcha)) {
            Mage::getSingleton('core/session')->addError('Por favor marque a opção "Não sou um robô".');

            return $this->_redirectReferer();
        }

        parent::postAction(); // change this to name of the method in question
    }
    ```
