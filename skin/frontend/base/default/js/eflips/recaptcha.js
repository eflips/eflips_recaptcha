window.addEventListener('load', function () {
  if (!document.getElementById('g-recaptcha-response')) {
    return // there is no recaptcha on current page
  }

  window.validateRecaptchaTimer = setInterval(function () {
    var recaptchaField = document.getElementById('g-recaptcha-response')

    if (!recaptchaField.value) {
      return // captcha isn't solved yet
    }

    var captchaValidatorField = document.getElementById('captcha-response-value')
    captchaValidatorField.value = recaptchaField.value

    if (captchaValidatorField.value !== '') {
      clearInterval(window.validateRecaptchaTimer)
    }

    return true
  }, 500)
})
