<?php

require_once "Mage/Contacts/controllers/IndexController.php";

class Eflips_Recaptcha_Contacts_IndexController extends Mage_Contacts_IndexController
{
    public function postDispatch()
    {
        parent::postDispatch();
        Mage::dispatchEvent('controller_action_postdispatch_adminhtml', array('controller_action' => $this));
    }

    /**
     * Validate (re)captcha before submit the contacts form
     */
    public function postAction()
    {
        $captcha = $this->getRequest()->getParam('g-recaptcha-response');

        if (!Mage::helper('recaptcha')->validateCaptcha($captcha)) {
            Mage::getSingleton('core/session')->addError('Por favor marque a opção "Não sou um robô".');

            return $this->_redirectReferer();
        }

        parent::postAction();
    }
}
