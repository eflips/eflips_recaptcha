<?php

class Eflips_Recaptcha_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function __get($configKey)
    {
        return Mage::getStoreConfig("recaptcha/general/$configKey");
    }

    /**
     * Validate if given reCaptcha response is valid
     * @param string $response Response to be used on validation
     * @return bool Captcha is valid or not
     */
    public function validateCaptcha($response)
    {
        if (empty($response)) {
            return false;
        }

        $verifyUrl = "https://www.google.com/recaptcha/api/siteverify?secret={$this->secret_key}&response=$response";
        $verifyResponse = file_get_contents($verifyUrl);
        $responseData = json_decode($verifyResponse);

        return (bool) $responseData->success;
    }
}
