<?php

class Eflips_Recaptcha_Block_Captcha extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        return '<li>
            <div class="g-recaptcha" data-sitekey="' . Mage::helper('recaptcha')->site_key . '"></div>
            <input type="hidden" id="captcha-response-value" class="required-entry">
        </li>';
    }
}
